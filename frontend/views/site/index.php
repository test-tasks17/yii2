<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="p-5 mb-4 bg-transparent rounded-3">
        <div class="container-fluid py-5 text-center">
            <h1 class="display-4">Привет!</h1>
            <p class="fs-5 fw-light">Странички с тестовым тут</p>
            <p><a class="btn btn-lg btn-success" href="/frontend/web/index.php/news">Новости</a></p>
            <p><a class="btn btn-lg btn-success" href="/backend/web/index.php">Админка</a></p>

        </div>
    </div>

    <div class="body-content">


    </div>
</div>
