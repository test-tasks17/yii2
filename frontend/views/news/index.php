<?php
/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Новости</h1>
 <br>
<?php foreach ($varInView as $item): ?>
 <div class="card" style="width:100%">
    <div class="card-body">
      <h4 class="card-title"><?php echo $item->name ?></h4>
      <p class="card-text"><?php echo $item->text ?></p>
    </div>
  </div>
  <br>
 <?php endforeach ?>

 <hr>

 <h1>Тест формы</h1>
 <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'fio') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'phone') ?>
    <div class="form-group">
        <?= Html::submitButton('Test', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end() ?>