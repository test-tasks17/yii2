<?php
namespace app\models;
use Yii;

class NewsList extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
    return 'news'; 
    }


    public static function getAll()
    {
    $data = self::find()->all();
    return $data;
    }

}