<?php
namespace app\models;
use Yii;
use yii\base\Model;

class NewsForm extends Model
{
    
    public $fio;
    public $email;
    public $phone;
 
    public function rules()
    {
        return [
            [['fio'], 'string', 'min' => 3],
            ['email', 'email'],
            [['phone'], 'string'],
            ['phone', 'match', 'pattern' => '/^(8)[(](\d{3})[)](\d{3})[-](\d{2})[-](\d{2})/', 'message' => 'Телефон должен быть в формате 8(XXX)XXX-XX-XX'],
        ];
    }
}