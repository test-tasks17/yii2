<?php
namespace frontend\controllers;
use yii\web\Controller;
use app\models\NewsList;
use app\models\NewsForm;


class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $array = NewsList::getAll();
        $model = new NewsForm();

        return $this->render('index',[
            'varInView' => $array,
            'model' => $model
        ]);
    }
    // action снизу не работает, представление не видит переменную model, поэтому запихнул всё в верхний. 
    // Я не знаю где я прокосячился, но попробовал всё, хотелось бы узнать что не так, скажите после ревью пж.

    // public function actionPage()
    // {
    //     $model = new NewsForm();
    //     return $this->render('index', ['model' => $model]);
    // } 
}
