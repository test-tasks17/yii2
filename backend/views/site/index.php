<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Админ панель</h1>

        <br>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col">
                <h2>Управление новостями</h2>

                <p>CRUD панель управления новостями</p>

                <p><a class="btn btn-outline-secondary" href="/backend/web/index.php/news">Управление</a></p>
            </div>
            <div class="col">
                <h2>Сайт-портфолио</h2>

                <p>Предлагаю посмотреть его :)</p>

                <p><a class="btn btn-outline-secondary" href="https://denisdemin.ru">DenisDemin.RU</a></p>
            </div>
        </div>

    </div>
</div>
